% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function U = tnu_create_input ...
    (dt, tstop, ff_mean, fb_mean, ff2_mean, ff_std, fb_std, ff2_std)
% Create an input signal containing two feed-forward and one feed-backward
% signal
%
% INPUTS:
% 
% dt            - Temporal resolution of the signal in sec
% tstop         - Length of the signal in sec
% ff_mean       - Mean of FF in sec
% fb_mean       - Mean of FB in sec
% ff2_mean      - Mean of FF2 in sec
% ff_std        - Standard deviation of FF in sec
% fb_std        - Standard deviation of FB in sec
% ff2_std       - Standard deviation of FF2 in sec
%
% OUTPUTS:
%
% U             - Struct, containing the three input signals
%   U.dt        - Temporal resolution of the signal
%   U.u         - Each column represents one input signal [FF, FB, FF2]
%

    t           = 0:dt:tstop;
    
    U.dt        = dt;    
    U.u         = zeros(length(t),3);
    
    FF          = min(round(ff_mean/U.dt),length(t));
    FB          = min(round(fb_mean/U.dt),length(t));
    FF2         = min(round(ff2_mean/U.dt),length(t));

    U.u(FF,1)   = 1;
    U.u(FB,2)   = 1;
    U.u(FF2,3)  = 1;

    U.u        = [ ...
        imgaussfilt(U.u(:,1),round(ff_std/U.dt), ...
        'Filtersize',1+round(ff_std/U.dt)*8), ...
        imgaussfilt(U.u(:,2),round(fb_std/U.dt), ...
        'Filtersize',1+round(fb_std/U.dt)*8), ...
        imgaussfilt(U.u(:,3),round(ff2_std/U.dt), ...
        'Filtersize',1+round(ff2_std/U.dt)*8)];
end
