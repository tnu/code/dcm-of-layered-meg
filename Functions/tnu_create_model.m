% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function M = tnu_create_model ...
    (u_dt,y_dt,tstop,dip_coord,dip_ori,dip_loc,dim_red,n_outputs)
% Creates a model and its inputs for a set of parameters
%
% u_dt                     - dt of state equation (in sec)
% y_dt                     - dt of output equation (in sec)
% tstop                    - point until which to simulate (in sec)
% dip_coord                - MNI coordinates to the dipole pair location
% dip_ori                  - Normal vector of dipipole pair orientation in
%                            MNI space
% dip_loc                  - Relative location of the two dipoles in MNI
%                            space (loc_L23 - loc_L5)
% dim_red                  - Define the kind of dimensionality reduction to
%                            be used. The possible types are:
%                 -) 'none'       No dimensionality reduction
%                 -) 'pca'        Principal Component Analysis
%                 -) 'subset'     Use only a subset of the sensors
%                 -) 'traces'     Create the traces directly. No dipoles
% n_outputs                - Number of outputs (optional)

    % Define parameter needed for SPM.
    n    = 1;    
    A{1} = 0;
    A{2} = 0;
    A{3} = sparse(n);
    B    = {};
    
    M.dt = u_dt;
    M.tstop = tstop;
    
    % Set important parameter
    M.dipfit.model = 'CMC';
    M.dipfit.type  = 'LFP';
    M.dipfit.Nc    = n;
    M.dipfit.Ns    = n;
    M.m   = 3;         % inputs
    M.n   = n*8;       % states
    
    % Define the scaling and the underlying temporal model
    C           = zeros(4,3);
    
    C(1,1)      = 1;
    C(1,3)      = 1;
    C(2,2)      = 1;
    C(3,2)      = 1;
    C(4,2)      = 1;
    
    C(1,2)      = 1;
    
    C           = reshape(C,1,12); % This needs to be a 1D vector
    
    M.scale     = 5000;
    M.g         = 'tnu_gx_cmc';
    
    % Calculate the neuronal priors
    [pE,pC]     = spm_dcm_neural_priors(A,B,C,M.dipfit.model);
    M.pE        = pE;
    M.pC        = pC; 
    
    % Get fx of the model (state equation)
    [~,f]       = spm_dcm_x_neural(M.pE,M.dipfit.model);
    M.f         = f;
    M.x        	= sparse(1,M.n);
    
    % Load the location and orientation of a brain and the location of the
    % sensors. This information is needed to calculate lead-fields.
    D           = spm_eeg_load( ...
        ['..' filesep 'Data' filesep 'mce1spmeeg_s01_Uni_Rep1.mat']);
    
    % get the locations and orientations of the vertex on the pial surface
    % and the white matter surface in sensor space coordinations
    pos_pial    = tnu_transform_MNI2sensor(D,dip_coord+dip_loc/2);
    pos_white   = tnu_transform_MNI2sensor(D,dip_coord-dip_loc/2);
    orientation = tnu_transform_MNI2sensor(D,dip_ori+dip_coord) - ...
        tnu_transform_MNI2sensor(D,dip_coord);
    orientation = orientation/norm(orientation);
    
    % Get the actual angle w.r.t. the radial component from the center of
    % the volume conductor
    [distance_from_center,radial_vec_center] = ...
        tnu_get_distance_MNI2center(D,dip_coord);
    radial_angle = acos(orientation*radial_vec_center'/ ...
        (norm(orientation)*norm(radial_vec_center')))*180/pi;
    fprintf('Distance to origin of volume conductor: %.2f.\n', ...
        distance_from_center);
    fprintf('Radial angle of dipole: %.2f deg.\n', ...
        radial_angle);
    
    % Calculate the lead-fields
    M.l_pial   	= tnu_get_leadfield(D,pos_pial,orientation);
    M.l_white   = tnu_get_leadfield(D,pos_white,orientation);
    
    % Define the output function
    switch dim_red
        case 'none'
            M.l = 274;
            M.g = 'tnu_gx_none';
            
        case 'subset_sum'
            try
                M.l = n_outputs;
            catch
                M.l = 16;
            end
            
            % Load the list of sorted dipoles
            load('../Data/dim_reduction_perm.mat');
            
            % Reduce the size of the lead-field vectors
            M.l_pial  = M.l_pial(perm_sum(1:M.l));
            M.l_white = M.l_white(perm_sum(1:M.l));
            
            M.g       = 'tnu_gx_none';
            
        case 'subset_diff'
            try
                M.l = n_outputs;
            catch
                M.l = 16;
            end
            
            % Load the list of sorted dipoles
            load('../Data/dim_reduction_perm.mat');
            
            % Reduce the size of the lead-field vectors
            M.l_pial  = M.l_pial(perm_diff(1:M.l));
            M.l_white = M.l_white(perm_diff(1:M.l));
            
            M.g       = 'tnu_gx_none';
        
        case 'traces'
            M.l       = 2;
            M.l_pial  = sparse([1;0]); 
            M.l_white = sparse([0;1]);
            
            M.g       = 'tnu_gx_none';
        
        otherwise
            error( ...
                'Unknown dimensionality reduction. Use "none" for no red.')
    end
    
    % Set the different time constant for the output
    M.delays    = zeros(M.l,1);
    M.ns        = 1+round(u_dt/y_dt*(length(0:u_dt:tstop)-1));
    M.Nmax      = 350;
    
    % Do not plot graphs
    M.nograph = 1;
end
