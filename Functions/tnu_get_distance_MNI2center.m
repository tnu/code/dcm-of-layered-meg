% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [distance,vec] = tnu_get_distance_MNI2center(D,mniCoords)

% For a given vector in MNI space, calculate the distance two the center
% of the volume conductor and return a vector that is perpendicular to the
% volume conductor
%
% INPUTS:
% 
% D               - EEG/MEG data object with field D.inv{1}.forward.fromMNI
% mniCoords       - [Nx3] vector containing N points in MNI space
%
% OUTPUTS:
%
% distance        - [N] Distance between the MNI points and the center
% vec             - [Nx3] Vectors that are perpendicular to the center
%

% Written: 200126 (Stephan IHLE)

% Get the center of the volume conductor in sensor coordinates. This is a
% shortened version of what is usually be done. Therefore, this likely only
% works for this specific case (i.e. singleshell, ...). To have a more 
% generalizable solution, please look at tnu_get_leadfield.m
forward     = D.inv{1}.forward;
[vol,~]     = ft_prepare_vol_sens(forward(1).vol,forward(1).sensors);
vec_CVC     = (vol.forwpar.center/vol.forwpar.scale)';

% Calculate the distance to the center in MNI space (Nx3)
vec_diff    = tnu_transform_MNI2sensor(D,mniCoords) - vec_CVC;
distance    = sqrt(sum(vec_diff.^2,2));

% Calculate the perpendicular vector for each coordinate point
vec         = vec_diff./distance;
end
