% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [L,D] = tnu_get_leadfield(D,vert,norm)
% Computes the gain matrix (lead-field) of a vertex with its given
% orientation.
%
% INPUT:
% 
% D                 - SPM MEEG structure (loaded with spm_eeg_load(...))
% vert              - (1x3) location of the dipole in sensor space
% norm              - (1x3) orientation of dipole in sensor space
%
% OUTPUT:
%
% L                 - Lead-field or gain matrix
%__________________________________________________________________________
% Copyright (C) 2008 Wellcome Trust Centre for Neuroimaging

% Karl Friston
% $Id: spm_eeg_lgainmat.m 6849 2016-07-31 12:34:33Z karl $

% Adapted by:
% Stephan IHLE and Jakob Heinzle


% get gain or lead-field matrix
%--------------------------------------------------------------------------
val = D.val;

forward = D.inv{val}.forward;

for ind = 1:numel(forward)
    modality = forward(ind).modality;
    
    % channels
    %----------------------------------------------------------------------
    if isequal(modality, 'MEG')
        chanind = D.indchantype({'MEG', 'MEGPLANAR'}, 'GOOD');
    else
        chanind = D.indchantype(modality, 'GOOD');
    end
    
    if ~isempty(chanind)
        forward(ind).channels = D.chanlabels(chanind);
    else
        error(['No good ' modality ' channels were found.']);
    end
end

channels = [forward(:).channels];

G     = {};
label = {};
for ind = 1:numel(forward)
    % create a new lead-field matrix
    %------------------------------------------------------------------

    vol  = forward(ind).vol;

    if ischar(vol)
        vol = ft_read_vol(vol);
    end

    modality = forward(ind).modality;

    if isfield(forward, 'siunits') && forward(ind).siunits
        units = D.units(D.indchannel(forward(ind).channels));
        sens  = forward(ind).sensors;
        siunits = isempty(strmatch('unknown', units));           
    else
        siunits = false;
        sens = D.inv{val}.datareg(ind).sensors;
    end            

    % Forward computation
    %------------------------------------------------------------------
    [vol, sens] = ft_prepare_vol_sens(vol, sens, 'channel', forward(ind).channels);
    nvert = size(vert, 1);

    spm('Pointer', 'Watch');drawnow;
    spm_progress_bar('Init', nvert, ['Computing ' modality ' leadfields']); drawnow;
    if nvert > 100, Ibar = floor(linspace(1, nvert,100));
    else Ibar = [1:nvert]; end

    if ~isequal(ft_voltype(vol), 'interpolate')
        Gxyz = zeros(length(forward(ind).channels), 3*nvert);
        for i = 1:nvert
            if siunits
                Gxyz(:, (3*i - 2):(3*i))  = ft_compute_leadfield(vert(i, :), sens, vol,...
                    'dipoleunit', 'nA*m', 'chanunit', units);
            else
                Gxyz(:, (3*i - 2):(3*i))  = ft_compute_leadfield(vert(i, :), sens, vol);
            end

            if ismember(i, Ibar)
                spm_progress_bar('Set', i); drawnow;
            end
        end
    else
        if siunits
            Gxyz = ft_compute_leadfield(vert, sens, vol, 'dipoleunit', 'nA*m', 'chanunit', units);
        else
            Gxyz = ft_compute_leadfield(vert, sens, vol);
        end
    end

    spm_progress_bar('Clear');
    spm_progress_bar('Init', nvert, ['Orienting ' modality ' leadfields']); drawnow;

    G{ind} = zeros(size(Gxyz, 1), size(Gxyz, 2)/3);
    for i = 1:nvert
        G{ind}(:, i) = Gxyz(:, (3*i- 2):(3*i))*norm(i, :)';
        if ismember(i,Ibar)
            spm_progress_bar('Set', i); drawnow;
        end

    end

    % condition the scaling of the lead-field
    %--------------------------------------------------------------------------        
    [Gs, scale] = spm_cond_units(G{ind});

    if siunits && abs(log10(scale))>2
        warning(['Scaling expected to be 1 for SI units, actual scaling ' num2str(scale)]);
        G{ind} = Gs;
    else
        scale = 1;
    end

    spm_progress_bar('Clear');

    spm('Pointer', 'Arrow');drawnow;

    label = [label; forward(ind).channels(:)];

    forward(ind).scale = scale;
end

if numel(G) > 1
    G = cat(1, G{:});
else
    G = G{1};
end

[sel1, sel2] = spm_match_str(channels, label);

if length(sel2) ~= numel(channels)
    error('Did not find a match for all the requested channels');
end

L   = sparse(G(sel2, :));

D.inv{val}.forward = forward;
