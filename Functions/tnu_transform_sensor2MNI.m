% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function mniCoords = tnu_transform_sensor2MNI(D,sensorCoords)

% Transform the MNI space points to new metric, containing 
%
% INPUTS:
% 
% D               - EEG/MEG data object with field D.inv{1}.forward.fromMNI
% sensorCoords    - Nx3 vector containing N points in MNI space
%
% OUTPUTS:
%
% mniCoords               - Nx3 vector containing same points in sensor space
%

% check whether field exists

if ~isfield(D.inv{1}.forward,'toMNI')
    error('could not find the needed transformation');
end

M = D.inv{1}.forward.toMNI;

mniCoords = M(1:3,1:4) * [sensorCoords';ones(1,length(sensorCoords(:,1)))];
mniCoords = mniCoords';

end
