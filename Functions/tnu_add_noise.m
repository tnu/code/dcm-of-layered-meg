% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [Y, level] = tnu_add_noise(Y,SNR,noise_kind)
% Add to a signal Y noise with a given SNR (looking at the variance)
% Y.dt          - Timing of signal
% Y.y       	- signal data
% SNR           - Signal to noise ratio (of variance of signals)
% noise_kind    - Kind of noise (optional)
%       'rel'   - The SNR is given in relationship to the signal (Default)
%       'abs'   - The SNR is given as an absolut value
%       'log'   - The SNR is given on a log scale as defined in
%                 Goldenholz et al. 2009 (10.1002/hbm.20571)
    if nargin < 3
        noise_kind = 'rel';
    end
    switch noise_kind
        case 'abs' % absolute
            level = SNR;
        case 'rel' % relative
            level = mean(var(Y.y)/SNR);
        case 'log' % logarithmic noise as defined in Goldenholz et al. 2009
            level = mean(max(Y.y.^2)) * 10^(SNR/-10)
        otherwise
            error('unkown noise kind')
    end
    noise   = (randn(size(Y.y))*sqrt(level));

    Y.y     = Y.y + noise;
end
