% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function y = tnu_gx_none(x,u,P,M)
% The output function for the CMCM using no dimensionality reduction.
%
% x     - current state of system (single time point)
% u     - current inputs of system (singe time point)
% P     - Model parameter (struct)
% M     - SPM model
%
% y     - output traces (single time point)
%
    y = [x(3) x(7)]*[M.l_pial M.l_white]'*M.scale;
end
