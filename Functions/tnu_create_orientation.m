% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function output = tnu_create_orientation(normal,plane_vec,angle)
% This function creates a vector that has a certain angle from a given
% normal vector.
% 
% INPUT:
%
% normal        - Normal vector used for reference of the angle
% plane_vec     - The output vector is in the plane created by this vector
%                 and the normal vector. Must not be in the same direction
%                 as the normal vector
% angle         - Angle (in degree) between the output vector and the
%                 normal vector
%
% OUTPUT:
%
% output        - Output vector
%
    % Normalize the normal vector
    normal = normal / norm(normal);
    
    % Transform the plane vector such that it is orthogonal to the normal
    % vector
    plane_vec = plane_vec - (plane_vec * normal') * normal;
    if norm(plane_vec) == 0
        error( ...
           'The normal and plane vector must not have the same direction');
    end
    
    % Normalize the 2nd plane vector
    plane_vec = plane_vec / norm(plane_vec);
    
    % Transform the angle from degree to radiant
    angle = angle * pi / 180;
    
    % Calculate the output vector
    output = cos(angle) * normal + sin(angle) * plane_vec;
end
    
