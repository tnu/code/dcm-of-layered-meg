SNR_level.mat
The mat contains the absolute SNR value that is used in the proof of concept figure at an SNR pf 0dB

CMCM_fitted_to_JCM_parameter.mat
A CMCM, that has been fitted to mimic the compartmental model of Jones et al. You can find in the README.md of the root directory, how it has been trained.

