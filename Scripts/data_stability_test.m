% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function data_stability_test(SNR,n,orientation,run)
% This code is doing a single simulation used for testing, if there are
% local minima during convergation. This is achieved by changing the
% starting position of spm_nlsi and looking at the results.
% 
% The Free Energy is saved in the following output file:
% ../Data/Stability_Test/Free_Energy.mat
% In the file there are three variables: F_LC, F_LD, and F_LI. They are an
% array containing the free energy of all runs (n) and the different SNRs
% (SNR). The first axis is used for the SNR, the second for the runs.
% 
% INPUT:
%
% SNR           - Signal to noise ratio (var(signal)/var(noise))
% n             - Noise trace (1, 2, ..., 20)
% orientation   - Define the Layer model (LC, LD, or LI):
%                   -) -1           - Layer Correct Model
%                   -) 0            - Layer Detached Model
%                   -) 1            - Layer Inverted Model
% Run           - (1, 2, ..., 10) used to create the random seed for the
%                 input of spm_nlsi (Ep)
% 

    %% Initizalization    
    % Add the SPM12 package and all other necessary directories to the
    % MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));

    % Initialize the random generator
    rng(n+100*SNR);

    % Change the step size for numerical derivatives
    global GLOBAL_DX
    GLOBAL_DX = exp(-5);
    
    % Define the possible SNRs and number of runs
    all_SNR = [10 6.667 3.333 0 -3.333 -6.667 -10 -13.333 -16.667 -20];
    all_n   = 20;
    
    % Define the results file filename
    file_results =  ['..' filesep 'Data' filesep 'Stability_Test' ...
       filesep 'SNR_' sprintf('%i',all_SNR(SNR)) ... 
       '__noise_' sprintf('%i',n) '__run_' sprintf('%i',run)];
    
    all_SNR(SNR)
    n
    run
    
    switch orientation
        case -1
            file_results = [file_results '__LI.mat'];
            disp('LI');
        case 0
            file_results = [file_results '__LD.mat'];
            disp('LD');
        case 1
            file_results = [file_results '__LC.mat'];
            disp('LC');
    end
    
    %% Define parameter
    u_dt      = 1/2400;  % Resolution of the state equation in sec
    y_dt      = 1/2400;  % Resolution of the observation equation in sec
    t_stop    = 0.160;   % Length of simulation in sec



    % MNI coordinates of the dipole pair
    dip_coord = [42 -29 56]; 

    % Orienation of the dipole pair. The information is given in form of a 
    % normal vector in the MNI coordinate system
    dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],60);

    % Distance between the two dipoles of a dipole pair in mm. The position
    % difference is given as (loc_L23 - loc_L5)
    dip_loc   = 2 * dip_ori / norm(dip_ori); 

    % Define the kind of dimensionality reduction used in this simulation
    dim_red   = 'none';

    %% Create the input signal
    ff_mean   = 0.025;   % Mean of FF in sec
    fb_mean   = 0.070;   % Mean of FB in sec
    ff2_mean  = 0.135;   % Mean of FF2 in sec

    ff_std    = 0.0025;  % Standard deviation of FF in sec
    fb_std    = 0.006;   % Standard deviation of FB in sec
    ff2_std   = 0.007;   % Standard deviation of FF2 in sec

    U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
        ff_std,fb_std,ff2_std);

    %% Create the model used for the simulation and inversion
    M_sim     = tnu_create_model(u_dt,y_dt,t_stop, ...
        dip_coord,dip_ori,dip_loc, ...
        dim_red);

    M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
        dip_coord,dip_ori,dip_loc*orientation, ...
        dim_red);
    
    % Load the data used for simulating the traces
    simulating_E = load(['..' filesep 'Data' filesep ...
        'Jones_Compartmental_Model' filesep ...
        'CMCM_fitted_to_JCM_parameter.mat']);
    simulating_E = simulating_E.Ep;
    
    
    %% Simulation
    Y.dt      = y_dt;
    Y.y       = spm_int(simulating_E,M_sim,U);
    
    % Add noise
    Y         = tnu_add_noise(Y,all_SNR(SNR));
    
    %% Create Starting Estimates
    rng(run*23);
    
    M_inv_pC_vec = spm_vec(M_inv.pC);
    random_vec   = randn(size(M_inv_pC_vec)) .* sqrt(M_inv_pC_vec);
    M_inv.P      = spm_unvec(random_vec,M_inv.pE);
    if size(spm_vec(M_inv.P)) ~= size(spm_vec(M_inv.pE))
        error('Intial vector has the wrong size');
    end
    
    rng(0);
    
    %% Inversion
    [Ep,Cp,Eh,F] = spm_nlsi_GN(M_inv,U,Y);
    F
    
    %% Save Data
    save(file_results,'Ep','Cp','Eh','F');
    
    %% Termination
    % Remove all directories from the MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));
end
