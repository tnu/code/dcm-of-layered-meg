% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script is used to sort the signal strength in all sensors. We need
% this to enable dimensionality reduction

%% Initialization

% Define the dipole angle
angle     = 60;

% Define the dipole coordination
dip_coord = [42 -29 56];

% Define the norm vector
norm_vec  = [1 0 0];

% Define the cortical thickness in mm
thickness = 2;

% -------------------------------------------------------------------------

% Add the SPM12 package and all other necessary directories to the
% MATLAB path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

% Define the dipole orientation
dip_ori   = tnu_create_orientation(dip_coord,norm_vec,angle);

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5)
dip_loc   = thickness * dip_ori / norm(dip_ori);

%% Order the sensors by their signal strength

% Create the model to get the LFPs
M         = tnu_create_model(1/2400,1/2400,0.16, ...
    dip_coord,dip_ori,dip_loc, ...
    'none');

% Get the LFP (sum)
LFP_sum         = abs(M.l_pial+M.l_white);
% Sort them by size
[~,perm_sum]    = sort(LFP_sum);
perm_sum        = flipud(perm_sum);

% Get the LFP (difference)
LFP_diff        = abs(M.l_pial-M.l_white);
[~,perm_diff]   = sort(LFP_diff);
perm_diff       = flipud(perm_diff);

%% Save the results
save('../Data/dim_reduction_perm.mat','perm_sum','perm_diff');

%% Termination

% Remove all directories from the MATLAB path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

