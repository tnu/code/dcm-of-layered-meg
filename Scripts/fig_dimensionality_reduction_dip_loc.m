% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script is plotting the location of the sensors in order used for the
% two dimensionality reduction approaches.

clear all;
close all;

% Add the SPM12 package and all other necessary directories to the
% MATLAB path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

%% Load the meeg object with the dipole locations
% Load the location and orientation of a brain and the location of the
% sensors. This information is needed to calculate lead-fields.
D               = spm_eeg_load( ...
    ['..' filesep 'Data' filesep 'mce1spmeeg_s01_Uni_Rep1.mat']);
chan_pos        = D.inv{1}.forward.sensors.chanpos;
chan_pos        = chan_pos(1:274,:);

%% Splatter out the points after normalizing z value
z_min           = min(chan_pos(:,3));
z_max           = max(chan_pos(:,3));
chan_pos(:,3)   = (chan_pos(:,3)-z_min)/(z_max-z_min)-1;
for i=1:size(chan_pos,1)
    chan_pos(i,:) = chan_pos(i,:) * sqrt(-chan_pos(i,3)+0.1);
end


%% Create color coding
gamma           = 0.9; % Factor defining the gray scale of the points
col             = zeros(size(chan_pos,1),3)+gamma;
for i=1:size(chan_pos,1)
    if i <= 1
        col_i = [1,0,0];
    elseif i <=2
        col_i = [1,1/2,0];
    elseif i <=3
        col_i = [1,2/3,0];
    elseif i <= 5
        col_i = [1,1,0];
    elseif i <= 9
        col_i = [0.5,1,0];
    elseif i <= 17
        col_i = [0,1,1];
    elseif i <= 29
        col_i = [0,0,1];
    elseif i <= 51
        col_i = [1,0,1];
    elseif i <= 89
        col_i = [0.7,0.3,0.7];
    elseif i <= 156
        col_i = [0.5,0.5,0.5];
    else
        col_i = [0,0,0];
    end
    col(i,:) = col_i;
end

% Order the color coding according to the two dim reduction methods
load('../Data/dim_reduction_perm.mat');
% perm_diff = perm_diff(1:24);
% perm_sum  = perm_sum(1:24);
% col       = col(1:24,:);

%% Plot the figure
figure('units','normalized','outerposition',[0 0 1 1]);

% Create Diff figure
ax = subplot(1,2,1);
scatter3(chan_pos(perm_diff,1),chan_pos(perm_diff,2),chan_pos(perm_diff,3),75,col,'filled');
view(-90,90);
colormap(ax,col(end:-1:1,:))
colorbar
axis('off')
title('Method: Difference Maximization','fontsize',18);

% Create Sum figure
ax = subplot(1,2,2);
scatter3(chan_pos(perm_sum,1),chan_pos(perm_sum,2),chan_pos(perm_sum,3),75,col,'filled');
view(-90,90);
colormap(ax,col(end:-1:1,:))
colorbar
axis('off')
title('Method: Sum Maximization','fontsize',18);

% Save Figure
saveas(gcf,'../Figures/dimensionality_reduction_sensor_order.png')
saveas(gcf,'../Figures/dimensionality_reduction_sensor_order.eps','epsc')
