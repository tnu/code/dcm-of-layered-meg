% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the Jones compartmental model traces for both the
% superficial and deep layer.
%
clear all
close all

% Load the data from the output data file created by the Jones model.
A  = load(['..' filesep 'Jones_et_al_Model' filesep 'Modified' filesep ...
    'suprathresh.dat']);
t  = A(:,1);
l2 = A(:,2);
l5 = A(:,3);

% Set the number of repetitions
n = 100;
if mod(size(A,1),n) ~= 0
    error('The number of repetitions (n) is not correct');
end

% Calculate the mean of the signals
t_JCM  = zeros(size(A,1)/n,1);
L2_JCM = zeros(size(A,1)/n,1);
L5_JCM = zeros(size(A,1)/n,1);
for i=1:n
    t_JCM = t_JCM + t(((i-1)*size(A,1)/n+1):(i*size(A,1)/n))/n/1000;
    L2_JCM = L2_JCM + l2(((i-1)*size(A,1)/n+1):(i*size(A,1)/n))/n/1000;
    L5_JCM = L5_JCM + l5(((i-1)*size(A,1)/n+1):(i*size(A,1)/n))/n/1000;
end

% Save the data in a file
% t_JCM  .. simulation time                 [ s ]
% L2_JCM .. measured signal from L2/3       [pAm]
% L5_JCM .. measured signal from L5         [pAm]
save(['..' filesep 'Data' filesep 'Jones_Compartmental_Model' filesep ...
    'output_traces.mat'], 't_JCM', 'L2_JCM', 'L5_JCM');
