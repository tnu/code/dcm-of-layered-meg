% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the sensor trace figures.

%% Initizalization
clear all
close all

% Add the SPM12 package and all other necessary directories to the MATLAB
% path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));
addpath(genpath('../fieldtrip-20180303/'));

% Initialize the random generator
rng(0);

% Change the step size for numerical derivatives
global GLOBAL_DX
GLOBAL_DX = exp(-5);

%% Define parameter
u_dt      = 0.0005;  % Resolution of the state equation in sec
y_dt      = 0.01;    % Resolution of the observation equation in sec
t_stop    = 0.160;   % Length of simulation in sec



% MNI coordinates of the dipole pair
dip_coord = [42 -29 56]; 

% Orienation of the dipole pair. The information is given in form of a 
% normal vector in the MNI coordinate system
dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],60);

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5)
dip_loc   = 4 * dip_ori / norm(dip_ori); 

% Define the kind of dimensionality reduction used in this simulation
dim_red   = 'none';

%% Create the input signal
ff_mean   = 0.025;   % Mean of FF in sec
fb_mean   = 0.070;   % Mean of FB in sec
ff2_mean  = 0.135;   % Mean of FF2 in sec

ff_std    = 0.0025;  % Standard deviation of FF in sec
fb_std    = 0.006;   % Standard deviation of FB in sec
ff2_std   = 0.007;   % Standard deviation of FF2 in sec

U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
    ff_std,fb_std,ff2_std);

%% Create the left and right model
M_left    = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,dip_loc, ...
    dim_red);

M_right    = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord.*[-1 1 1],dip_ori.*[-1 1 1],dip_loc.*[-1 1 1], ...
    dim_red);

%% Get the spm meeg object
D        = spm_eeg_load( ...
	['..' filesep 'Data' filesep 'mce1spmeeg_s01_Uni_Rep1.mat']);

%% Get all the sensor values
left_pial   = full(M_left.l_pial);
left_white  = full(M_left.l_white);
right_pial  = full(M_right.l_pial);
right_white = full(M_right.l_white);

%% Get the tf data object
d_tf        = D.fttimelock; % This creates a struct that can be adapted to 
                            % show the data one wants to show.
d_tf.trialinfo = [1];
d_tf.time      = 1:6;       % 1: lp
                            % 2: lw
                            % 3: rp
                            % 4: rw
                            % 5: delta_l
                            % 6: delta_p

d_tf.trial     = zeros(1,316,length(d_tf.time));
d_tf.trial(1,33:306,1) = left_pial;
d_tf.trial(1,33:306,2) = left_white;
d_tf.trial(1,33:306,3) = right_pial;
d_tf.trial(1,33:306,4) = right_white;
d_tf.trial(1,33:306,5) = left_pial - left_white;
d_tf.trial(1,33:306,6) = right_pial - right_white;

%% Create the topoplot object
cfg             = [];
cfg.trials      = 1;
cfg.colormap    = 'jet';
cfg.marker      = 'on';
cfg.comment     = 'no';
cfg.colorbar    = 'yes';
cfg.interactive = 'no';
cfg.zlim        = [-1 1]*ceil(max(max(abs(d_tf.trial(1,:,1:4)))));
cfg.layout      = 'CTF275.lay';

%% Plot the results of the 4 dipoles
figure('units','normalized','outerposition',[0 0 0.8 1])

subplot(2,2,1);
cfg.xlim = [1 1];
ft_topoplotER(cfg,d_tf);
title('Pial BA3b left');
subplot(2,2,2);
cfg.xlim = [2 2];
ft_topoplotER(cfg,d_tf);
title('White BA3b left');
subplot(2,2,3);
cfg.xlim = [3 3];
ft_topoplotER(cfg,d_tf);
title('Pial BA3b right');
subplot(2,2,4);
cfg.xlim = [4 4];
ft_topoplotER(cfg,d_tf);
title('White BA3b right');

%% Save the figure
saveas(gcf,'../Figures/spatial_sensor_traces.png')
saveas(gcf,'../Figures/spatial_sensor_traces.eps','epsc')

%% Plot the results for the difference traces
figure('units','normalized','outerposition',[0 0 1 1])

cfg.zlim        = [-1 1]*ceil(max(max(abs(d_tf.trial(1,:,5:6)))));

subplot(1,2,1);
cfg.xlim = [5 5];
ft_topoplotER(cfg,d_tf);
title('Pial BA3b left - white BA3b left')
subplot(1,2,2);
cfg.xlim = [6 6];
ft_topoplotER(cfg,d_tf);
title('Pial BA3b right - white BA3b right')

%% Save the figure
saveas(gcf,'../Figures/delta_sensor_traces.png')
saveas(gcf,'../Figures/delta_sensor_traces.eps','epsc')

%% Create Topoplots of the simulated data
u_dt      = 1/2400;  % Resolution of the state equation in sec
y_dt      = 1/2400;  % Resolution of the observation equation in sec
t_stop    = 0.160;   % Length of simulation in sec

% MNI coordinates of the dipole pair
dip_coord = [42 -29 56]; 

% Orienation of the dipole pair. The information is given in form of a 
% normal vector in the MNI coordinate system
dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],60);

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5)
dip_loc   = 2 * dip_ori / norm(dip_ori); 

% Define the kind of dimensionality reduction used in this simulation
dim_red   = 'none';

%% Create the input signal
ff_mean   = 0.025;   % Mean of FF in sec
fb_mean   = 0.070;   % Mean of FB in sec
ff2_mean  = 0.135;   % Mean of FF2 in sec

ff_std    = 0.0025;  % Standard deviation of FF in sec
fb_std    = 0.006;   % Standard deviation of FB in sec
ff2_std   = 0.007;   % Standard deviation of FF2 in sec

U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
    ff_std,fb_std,ff2_std);

%% Create the model used for the simulation and inversion
M         = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,dip_loc, ...
    dim_red);

% Load the data used for simulating the traces
simulating_E = load(['..' filesep 'Data' filesep ...
    'Jones_Compartmental_Model' filesep ...
    'CMCM_fitted_to_JCM_parameter.mat']);
simulating_E = simulating_E.Ep;

%% Simulation
Y.dt      = y_dt;
Y.y       = spm_int(simulating_E,M,U);

%% Create the tf object
real_tf      = D.fttimelock; % This creates a struct that can be adapted to 
                            % show the data one wants to show.

real_tf.trialinfo = [1];
real_tf.time      = (0:(size(Y.y,1)-1))*Y.dt;

real_tf.trial     = zeros(1,316,length(real_tf.time));
real_tf.trial(1,33:306,:) = Y.y';

%% Create the topoplot object
cfg             = [];
cfg.trials      = 1;
cfg.colormap    = 'jet';
cfg.marker      = 'off';
cfg.comment     = 'no';
cfg.colorbar    = 'yes';
cfg.interactive = 'no';
cfg.style       = 'straight';
cfg.zlim        = [-1 1]*ceil(max(max(abs(real_tf.trial(1,:,:)))));
cfg.layout      = 'CTF275.lay';

%% Plot the results of the 4 dipoles
figure('units','normalized','outerposition',[0 0 1 1])

time_points = [0 20 25 50 70 100 135 160];

for i=1:length(time_points)
    subplot(2,4,i);
    cfg.xlim = [1 1]*time_points(i)/1000;
    ft_topoplotER(cfg,real_tf);
    title(sprintf('%i ms',time_points(i)));
end


%% Save the figure
saveas(gcf,'../Figures/spatio-temporal_sensor_traces.png')
saveas(gcf,'../Figures/spatio-temporal_sensor_traces.eps','epsc')
