% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the proof_of_concept figure. The data first has to be
% created by the ../Euler/proof_of_concept.sh script, which is using the
% data_proof_of_concept.m function.
%
clear all
close all

% Define all the SNRs and number of runs
all_SNR = [10 6.667 3.333 0 -3.333 -6.667 -10 -13.333 -16.667 -20];
all_n   = 20;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The SNRs
% 2. dimension: The runs
% 3. dimension: The layer model (LC, LD, and LI)
Free_Energy = zeros(length(all_SNR),all_n,3);

% Load all the files containing the free energy and save them
l_model = {'LC','LD','LI'};
for i=1:length(all_SNR)
    for j=1:all_n
        for k=1:3
            file_results =  ['..' filesep 'Data' filesep ...
                'Proof_Of_Concept' filesep 'SNR_' ...
                sprintf('%i',all_SNR(i)) '__run_' sprintf('%i',j) ...
                '__' l_model{k} '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
            end
        end
    end
end

% Calculate the difference: F_LC-F_LD F_LI-F_LD:
F_LCLD = zeros(length(all_SNR),all_n);
F_LCLI = zeros(length(all_SNR),all_n);

for i=1:length(all_SNR)
    for j=1:all_n
        F_LCLD(i,j) = Free_Energy(i,j,1) - Free_Energy(i,j,2);
        F_LCLI(i,j) = Free_Energy(i,j,1) - Free_Energy(i,j,3);
    end
end

% Calculate the mean and std of the relative Free Energy
m_LILC   = zeros(length(all_SNR),1);
m_LILD   = zeros(length(all_SNR),1);
std_LILC = zeros(length(all_SNR),1);
std_LILD = zeros(length(all_SNR),1);
for i=1:length(all_SNR)
    m_LILC(i)   = mean(F_LCLD(i,:));
    m_LILD(i)   = mean(F_LCLI(i,:));
    std_LILC(i) = std(F_LCLD(i,:));
    std_LILD(i) = std(F_LCLI(i,:));
end

% Merge together the mean and std relative free energies
std_f           = [std_LILD, std_LILC];
mean_f          = [m_LILD, m_LILC];

% Open a Fullscreen figure to plot the results
figure('units','normalized','outerposition',[0 0 1 1])

% Define, which elements will be plotted in what subplot
t1         = 3;
t2         = 7;
indices    = cell(3,1);
indices{1} = 1:t1;
indices{2} = (t1+1):t2;
indices{3} = (t2+1):length(all_SNR);

% Plot the results in sub figures
for i=1:length(indices)
    subplot(1,length(indices),i);

    barwitherr(std_f(indices{i},:),mean_f(indices{i},:));

    set(gca,'XTickLabel',all_SNR(indices{i}))
    set(gca,'fontsize',16)
    xlabel('SNR')
    
    if i==1
        ylabel('{\Delta}F')
        
    elseif i==length(indices)-1
        h = hline(-3,'r:')
        set(h,'Linewidth',1.5)
        
    elseif i==length(indices)
        legend('F_{LC} - F_{LI}','F_{LC} - F_{LD}');
    end
    set(gca,'ygrid','on')
    h = hline(3,'r:')
    set(h,'Linewidth',1.5)
    
    x_lim = xlim;
    y_lim = ylim;
    rectangle(...
        'Position', [-5 -3 10 6], ...
        'FaceColor', [1 0 0 0.1], ...
        'EdgeColor', [0 0 0 0])
    xlim(x_lim);
    ylim(y_lim);
    
end
h = suptitle('Simulated Data at BA3b with Different SNR');
set(h,'FontSize',24);
%set(gca, 'YTickLabel', num2str(get(gca, 'YTick')'))

%% Save the plot
saveas(gcf,'../Data/Proof_Of_Concept/proof_of_concept.png')
saveas(gcf,'../Figures/proof_of_concept.png')
saveas(gcf,'../Figures/proof_of_concept.eps','epsc')
