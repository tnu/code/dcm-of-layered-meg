% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the dimensionality_reduction figure. The data first 
% has to be created by the ../Euler/dimensionality_reduciton.sh script, 
% which is using the data_dimensionality_reduction.m function.
%
clear all
close all

% Define all the SNRs and number of runs
all_dim = round(exp(0:log(274)/10:log(274)));
all_n   = 20;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The Dimensionality
% 2. dimension: The runs
% 3. dimension: The layer model (LC and LI)
% 4. dimension: The type of reduction
Free_Energy = zeros(length(all_dim),all_n,2,2);

% Load all the files containing the free energy and save them
l_model = {'LC','LI'};
for i=1:length(all_dim)
    for j=1:all_n
        for k=1:2
            for l=1:2
                file_results =  ['..' filesep 'Data' filesep ...
                    'Dimensionality_Reduction_3' filesep 'Dim_' ...
                    sprintf('%i',all_dim(i)) '__run_' sprintf('%i',j) ...
                    '__reduction_type_' sprintf('%i',l-1) ...
                    '__SNR_dB_-10__' l_model{k} '.mat'];
                try
                    load(file_results);
                    Free_Energy(i,j,k,l) = F;
                catch
                    [i j k l]
                    file_results
                end
            end
        end
    end
end

% Calculate the difference: F_LC-F_LI:
F_LCLI = zeros(length(all_dim),all_n,2);
for i=1:length(all_dim)
    for j=1:all_n
        F_LCLI(i,j,:) = Free_Energy(i,j,1,:) - Free_Energy(i,j,2,:);
    end
end

% Calculate the mean and std of the relative Free Energy
mean_f = zeros(length(all_dim),2);
std_f  = zeros(length(all_dim),2);
for i=1:length(all_dim)
    mean_f(i,1)   = mean(F_LCLI(i,:,1));
    std_f(i,1)    = std(F_LCLI(i,:,1));
    mean_f(i,2)   = mean(F_LCLI(i,:,2));
    std_f(i,2)    = std(F_LCLI(i,:,2));
end

% Open a Fullscreen figure to plot the results
figure('units','normalized','outerposition',[0 0 1 1])

% Define, which elements will be plotted in what subplot


barwitherr(std_f,mean_f);
legend('Maximize: ||LFP_{2/3}||_2 + ||LFP_{5}||_2',...
       'Maximize: ||LFP_{2/3} - LFP_{5}||_2')

set(gca,'XTickLabel',all_dim)
set(gca,'fontsize',16)
xlabel('Number of used Sensors Traces')
ylabel('{\Delta}F')
h = hline(3,'r:')
set(h,'linewidth',2)
h = hline(-3,'r:')
set(h,'linewidth',2)
set(gca,'ygrid','on')
    
h = title('Dimensionality Reduction (-10 dB)');
set(h,'FontSize',24);

x_lim = xlim;
y_lim = ylim;
rectangle('Position', [-20 -3 40 6], 'FaceColor', [1 0 0 0.1], 'EdgeColor', [0 0 0 0])
xlim(x_lim);
ylim(y_lim);

%set(gca, 'YTickLabel', num2str(get(gca, 'YTick')'))

%% Save the plot
% saveas(gcf,'../Data/Dimensionality_Reduction_3/dim_red_m10_dB.png')
% saveas(gcf,'../Figures/dimensionality_reduction_m10_dB.png')
% saveas(gcf,'../Figures/dimensionality_reduction_m10_dB.eps','epsc')
