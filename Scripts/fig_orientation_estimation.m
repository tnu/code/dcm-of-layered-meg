% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the orientation_estimation figure. The data first has
% to be created by the ../Euler/orientation_estimation.sh script, which is 
% using the orientation_estimation.m function.
%
clear all
close all

% Define all the SNRs and number of runs
all_mode = 4;
all_n    = 20;
all_dd   = -4:0.5:4;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The mode (1, 2, 3, and 4)
% 2. dimension: The runs
% 3. dimension: The dipole distance)
Free_Energy = zeros(all_mode,all_n,length(all_dd));

% Load all the files containing the free energy and save them
for i=1:all_mode
    for j=1:all_n
        for k=1:length(all_dd)
            file_results =  ['..' filesep 'Data' filesep ...
                'Orientation_Estimation' filesep 'Mode_' ...
                sprintf('%i',i) '__run_' sprintf('%i',j) ...
                '__distance_' sprintf('%.1f',all_dd(k)) '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
                if j > 1
                    Free_Energy(i,j,k) = Free_Energy(i,j-1,k);
                else
                    warning('Could not add a value here');
                end
            end
        end
    end
end


% Calculate the difference between the 2mm model and the other models
delta_f = zeros(size(Free_Energy));
pos_0   = find(all_dd==2); % Position in Free energy, where dd = 2mm
for i=1:all_mode
    for j=1:all_n
        for k=1:length(all_dd)
            delta_f(i,j,k) = Free_Energy(i,j,k) - Free_Energy(1,j,pos_0);
        end
    end
end

% Calculate the std and mean of the free energy
m_free_energy   = zeros(length(all_dd),all_mode);
s_free_energy   = zeros(length(all_dd),all_mode);
for i=1:all_mode
 	for k=1:length(all_dd)
        m_free_energy(k,i) = mean(delta_f(i,:,k));
        s_free_energy(k,i) = std(delta_f(i,:,k))/sqrt(2-1);
	end
end

%% Plot the model

figure('units','normalized','outerposition',[0 0 1 1])

subplot(3,1,1);
hold on
h = fill([all_dd fliplr(all_dd)], ...
        [m_free_energy(:,1)'+s_free_energy(:,1)' ...
        fliplr(m_free_energy(:,1)'-s_free_energy(:,1)')], ...
        'r', 'linestyle', 'none')
set(h,'facealpha',.1)
plot(all_dd,m_free_energy(:,1)','r-','Linewidth',2);
set(gca,'ygrid','on')
xlim([all_dd(1),all_dd(end)]);
set(gca,'fontsize',16);
ylim([-60 10]);
set(gca,'xticklabel',{[]}) 

subplot(3,1,2);
hold on
h = fill([all_dd fliplr(all_dd)], ...
        [m_free_energy(:,4)'+s_free_energy(:,4)' ...
        fliplr(m_free_energy(:,4)'-s_free_energy(:,4)')], ...
        'g', 'linestyle', 'none')
set(h,'facealpha',.1)
plot(all_dd,m_free_energy(:,4)','g-','Linewidth',2);
set(gca,'ygrid','on')
xlim([all_dd(1),all_dd(end)]);
set(gca,'fontsize',16);
%ylim([-1500 -800]);
set(gca,'xticklabel',{[]}) 
ylabel('{\Delta}F');

subplot(3,1,3);
hold on
plot([0 0],[1 1],'r-','Linewidth',2)
plot([0 0],[1 1],'y-','Linewidth',2)
plot([0 0],[1 1],'b-','Linewidth',2)
plot([0 0],[1 1],'g-','Linewidth',2)
h = fill([all_dd fliplr(all_dd)], ...
        [m_free_energy(:,3)'+s_free_energy(:,3)' ...
        fliplr(m_free_energy(:,3)'-s_free_energy(:,3)')], ...
        'b', 'linestyle', 'none')
set(h,'facealpha',.1)
h = fill([all_dd fliplr(all_dd)], ...
        [m_free_energy(:,2)'+s_free_energy(:,2)' ...
        fliplr(m_free_energy(:,2)'-s_free_energy(:,2)')], ...
        'y', 'linestyle', 'none')
set(h,'facealpha',.2)
plot(all_dd,m_free_energy(:,2)','y-','Linewidth',2);
plot(all_dd,m_free_energy(:,3)','b-','Linewidth',2);
set(gca,'ygrid','on')
xlim([all_dd(1),all_dd(end)]);
set(gca,'fontsize',16);
ylim([-13000 -5000]);
xlabel('Dipole Distance [mm]');

%   	1: Correct Dipole, Correct Orientation
%     	2: Correct Dipole, Wrong Orientation     
%     	3: Wrong Dipole, Correct Orientation    
%      	4: Wrong Dipole, Wrong Orientation      
legend('correct dipole, neg cm', ...
    'correct dipole, pos cm', ...
    'wrong dipole, pos cm', ...
    'wrong dipole, neg cm')
    

%% Save the plot
% saveas(gcf,'../Data/Orientation_Estimation/orientation_estimation.png')
% saveas(gcf,'../Figures/orientation_estimation.png')
saveas(gcf,'../Figures/orientation_estimation.eps','epsc')
