% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function data_orientation_estimation(mode,n,distance)
% This code is doing a single simulation used for the
% orientation_estimation figure. The mode, the noise signal (depenend on n) 
% and the dipole distance can be defined. The SNR is fixed for this
% simulation.
% 
% The Free Energy is saved in the following output file:
% ../Data/Orientation_Estimation/...
% 
% INPUT:
%
% mode          - Mode used for the simulation. 4 modes exist:
%                   1: Correct Dipole, Correct Orientation
%                   2: Correct Dipole, Wrong Orientation
%                   3: Wrong Dipole, Correct Orientation
%                   4: Wrong Dipole, Wrong Orientation
% n             - Noise trace (1, 2, ..., 20)
% distance      - The distance between Layer II/III and Layer V in mm. If
%                 the value is positive, then the layer ordering is correct
% 

    %% Initizalization    
    % Add the SPM12 package and all other necessary directories to the
    % MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));

    % Initialize the random generator
    rng(n);

    % Change the step size for numerical derivatives
    global GLOBAL_DX
    GLOBAL_DX = exp(-5);
    
    % Define the possible SNRs and number of runs
    all_dd  = -4:0.5:4;
    
    % Define the results file filename
    file_results =  ['..' filesep 'Data' filesep ...
        'Orientation_Estimation' filesep 'Mode_'  ...
        sprintf('%i',mode) '__run_' ...
        sprintf('%i__distance_',n) sprintf('%.1f.mat',all_dd(distance))];
    
    SNR       = 0
    
    real_mode = mode
    real_n    = n
    real_dd   = all_dd(distance)
    
    %% Define parameter
    u_dt      = 1/2400;  % Resolution of the state equation in sec
    y_dt      = 1/2400;  % Resolution of the observation equation in sec
    t_stop    = 0.160;   % Length of simulation in sec



    % MNI coordinates of the dipole pair
    dip_coord = [42 -29 56]; 

    % Orienation of the dipole pair. The information is given in form of a 
    % normal vector in the MNI coordinate system
    dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],30);

    % Distance between the two dipoles of a dipole pair in mm. The position
    % difference is given as (loc_L23 - loc_L5)
    dip_loc   = 1 * dip_ori / norm(dip_ori); 

    % Define the kind of dimensionality reduction used in this simulation
    dim_red   = 'none';
    
    % Define the distance between the two dipole pairs
    dp_dist   = 7 * dip_ori / norm(dip_ori);
    
    % Calculate the dipole coordinates, the dipole orientation, and the 
    % dipole location for all 4 modes
    dip1_coord 	= dip_coord + dp_dist/2;
    dip2_coord 	= dip_coord + dp_dist/2;
    dip3_coord 	= dip_coord - dp_dist/2;
    dip4_coord 	= dip_coord - dp_dist/2;
    
    dip1_loc    = + dip_loc;
    dip2_loc    = + dip_loc;
    dip3_loc    = - dip_loc;
    dip4_loc    = - dip_loc;
    
    dip1_ori    = + dip_ori;
    dip2_ori    = - dip_ori;
    dip3_ori    = - dip_ori;   % correct direction and opposite side
    dip4_ori    = + dip_ori;   % wrong direction and opposite side 
    

    %% Create the input signal
    ff_mean   = 0.025;   % Mean of FF in sec
    fb_mean   = 0.070;   % Mean of FB in sec
    ff2_mean  = 0.135;   % Mean of FF2 in sec

    ff_std    = 0.0025;  % Standard deviation of FF in sec
    fb_std    = 0.006;   % Standard deviation of FB in sec
    ff2_std   = 0.007;   % Standard deviation of FF2 in sec

    U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
        ff_std,fb_std,ff2_std);

    %% Create the model used for the simulation and inversion
    M_sim     = tnu_create_model(u_dt,y_dt,t_stop, ...
        dip1_coord,dip1_ori,dip1_loc*2, ...
        dim_red);
    switch mode
        case 1
            M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
                dip1_coord,dip1_ori,dip1_loc*all_dd(distance), ...
                dim_red);
        case 2
            M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
                dip2_coord,dip2_ori,dip2_loc*all_dd(distance), ...
                dim_red);
        case 3
            M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
                dip3_coord,dip3_ori,dip3_loc*all_dd(distance), ...
                dim_red);
        case 4
            M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
                dip4_coord,dip4_ori,dip4_loc*all_dd(distance), ...
                dim_red);
    end
    
    % Load the data used for simulating the traces
    simulating_E = load(['..' filesep 'Data' filesep ...
        'Jones_Compartmental_Model' filesep ...
        'CMCM_fitted_to_JCM_parameter.mat']);
    simulating_E = simulating_E.Ep;
    
    
    %% Simulation
    Y.dt      = y_dt;
    Y.y       = spm_int(simulating_E,M_sim,U);
    
    % Add noise
    Y         = tnu_add_noise(Y,SNR,'log');
    
    %% Inversion
    [Ep,Cp,Eh,F] = spm_nlsi_GN(M_inv,U,Y);
    
    %% Save Data
    save(file_results,'Ep','Cp','Eh','F','real_mode','real_n','real_dd');
    
    %% Termination
    % Remove all directories from the MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));
end
