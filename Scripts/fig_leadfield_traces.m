% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script is used to create the leadfied_traces figure.
clear all
close all

% Add the SPM12 package and all other necessary directories to the MATLAB
% path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

%% Define parameter
u_dt      = 0.0005;  % Resolution of the state equation in sec
y_dt      = 0.01;    % Resolution of the observation equation in sec
t_stop    = 0.160;   % Length of simulation in sec



% MNI coordinates of the dipole pair
dip_coord = [42 -29 56]; 

% Orienation of the dipole pair. The information is given in form of a 
% normal vector in the MNI coordinate system
dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],60);

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5)
dip_loc   = 4 * dip_ori / norm(dip_ori); 

% Define the kind of dimensionality reduction used in this simulation
dim_red   = 'none';

%% Create two models of BA3b (one for each side)

M_1       = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,dip_loc, ...
    dim_red);

M_2       = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord.*[-1 1 1],dip_ori.*[-1 1 1],dip_loc.*[-1 1 1], ...
    dim_red);

%% Plot the traces

% Plot the traces
figure('units','normalized','outerposition',[0 0 1 1]);
hold on
plot(M_1.l_pial,'y-','LineWidth',2);
plot(M_1.l_white,'g.-','LineWidth',2);
plot(M_2.l_pial,'b--','LineWidth',2);
plot(M_2.l_white,'r:','LineWidth',2);

% Make graph look nicer
box off
grid on 
set(gca,'fontsize',16)
xlabel('Sensor Number');
ylabel('Signal Strength');
title('Dipole Traces of two Dipole Pairs in Sensor Space');
legend('Pial Dipole in left BA3b', ...
    'White Matter Dipole in left BA3b', ...
    'Pial Dipole in right BA3b', ...
    'White Matter Dipole in right BA3b');
axis([0 274 -6 6]);

%% Save the figure

saveas(gcf,'../Figures/leadfield_traces.png');
saveas(gcf,'../Figures/leadfield_traces.eps','epsc');

%% Termination

% Remove all directories from the MATLAB path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));
