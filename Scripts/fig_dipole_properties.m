% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the dipole_properties figure. The data first has to
% be created by the following scripts:
% -) ../Euler/angle.sh
% -) ../Euler/distance.sh
% -) ../Euler/cortical_thickness.sh
% -) ../Euler/lfp_norm.sh
%
clear all
close all

%% Load the cortical_thickness data

% Define all the thicknesses and number of runs
all_thickness = 0:1/3:4;
all_n   = 20;

% Load the necessary TNU functions
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The cortical thickness
% 2. dimension: The runs
% 3. dimension: The layer model
Free_Energy = zeros(length(all_thickness),all_n,2);

% Load all the files containing the free energy and save them
l_model = {'LC','LI'};
for i=1:length(all_thickness)
    for j=1:all_n
        for k=1:2
            file_results =  ['..' filesep 'Data' filesep ...
                'Dipole_Properties' filesep ...
                'Cortical_Thickness' filesep  'Thickness_' ...
                sprintf('%.1f',all_thickness(i)) ...
                '__run_' sprintf('%i',j) '__' l_model{k} '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
                if j > 1
                    Free_Energy(i,j,k) = Free_Energy(i,j-1,k);
                end
            end
        end
    end
end


% Calculate the difference between the LC and LI model
delta_f = Free_Energy(:,:,1) - Free_Energy(:,:,2);

% Calculate the std and mean of the free energy
m_free_energy   = zeros(length(all_thickness),1);
s_free_energy   = zeros(length(all_thickness),1);
for i=1:length(all_thickness)
 	m_free_energy(i) = mean(delta_f(i,:));
    s_free_energy(i) = std(delta_f(i,:));
end

%% Plot the cortical_thickness subplot

figure('units','normalized','outerposition',[0 0 1 1])

subplot(3,1,1);
fill([all_thickness fliplr(all_thickness)], ...
    [m_free_energy'+s_free_energy' ...
    fliplr(m_free_energy'-s_free_energy')], ...
    [.95 .95 .95], 'linestyle', 'none')
hold all
plot(all_thickness,m_free_energy','r-','Linewidth',2);
grid on
ylabel('{\Delta}F');
xlabel('Cortical Thickness [mm]');
xlim([all_thickness(1),all_thickness(end)]);

% Get the y limit
y_lim = ylim;
v = hline(3,'g:');
set(v,'linewidth',2);
hline(0,'k-');
h = vline(2,'b--');
set(h,'linewidth',2);
ylim(y_lim);
set(gca,'fontsize',16);

%% Load the angle data

% Define all the thicknesses and number of runs
all_angle = 0:6:90;
all_n   = 20;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The cortical thickness
% 2. dimension: The runs
% 3. dimension: The layer model
Free_Energy = zeros(length(all_angle),all_n,2);

% Load all the files containing the free energy and save them
l_model = {'LC','LI'};
for i=1:length(all_angle)
    for j=1:all_n
        for k=1:2
            file_results =  ['..' filesep 'Data' filesep ...
                'Dipole_Properties' filesep ...
                'Angle' filesep  'Angle_' ...
                sprintf('%i',all_angle(i)) ...
                '__run_' sprintf('%i',j) '__' l_model{k} '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
                if j > 1
                    Free_Energy(i,j,k) = Free_Energy(i,j-1,k);
                end
            end
        end
    end
end

% Calculate the difference between the LC and LI model
delta_f = Free_Energy(:,:,1) - Free_Energy(:,:,2);

% Calculate the std and mean of the free energy
m_free_energy   = zeros(length(all_angle),1);
s_free_energy   = zeros(length(all_angle),1);
for i=1:length(all_angle)
 	m_free_energy(i) = mean(delta_f(i,:));
    s_free_energy(i) = std(delta_f(i,:));
end

%% Transform angles into the radial angles wrt the center of volume cond.
all_angle_center = all_angle;

% MNI coordinates of the dipole pair
dip_coord   = [42 -29 56]; 

% Load the location and orientation of a brain and the location of the
% sensors. This information is needed to calculate lead-fields.
D           = spm_eeg_load( ...
    ['..' filesep 'Data' filesep 'mce1spmeeg_s01_Uni_Rep1.mat']);

for i=1:size(all_angle,2)

    % Orienation of the dipole pair. The information is given in form of a 
    % normal vector in the MNI coordinate system
    dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],all_angle(i));

    % Distance between the two dipoles of a dipole pair in mm. The position
    % difference is given as (loc_L23 - loc_L5)
    dip_loc   = 2 * dip_ori / norm(dip_ori); 

    pos_pial    = tnu_transform_MNI2sensor(D,dip_coord+dip_loc/2);
    pos_white   = tnu_transform_MNI2sensor(D,dip_coord-dip_loc/2);
    orientation = tnu_transform_MNI2sensor(D,dip_ori+dip_coord) - ...
        tnu_transform_MNI2sensor(D,dip_coord);
    orientation = orientation/norm(orientation);
    
    % Get the actual angle w.r.t. the radial component from the center of
    % the volume conductor
    [distance_from_center,radial_vec_center] = ...
        tnu_get_distance_MNI2center(D,dip_coord);
    radial_angle = acos(orientation*radial_vec_center'/ ...
        (norm(orientation)*norm(radial_vec_center')))*180/pi;
    
    all_angle_center(i) = radial_angle;   
end

% Resort the angles
[~,perm]         = sort(all_angle_center);
all_angle_center = all_angle_center(perm);
m_free_energy    = m_free_energy(perm);
s_free_energy    = s_free_energy(perm);


%% Plot the angle subplot

subplot(3,1,2);
fill([all_angle_center fliplr(all_angle_center)], ...
    [m_free_energy'+s_free_energy' ...
    fliplr(m_free_energy'-s_free_energy')], ...
    [.95 .95 .95], 'linestyle', 'none')
hold all
plot(all_angle_center,m_free_energy','r-','Linewidth',2);
grid on
ylabel('{\Delta}F');
xlabel('Angle [degree]');
xlim([all_angle_center(1),all_angle_center(end)]);

% Get the y limit
y_lim = ylim;
v = hline(3,'g:');
set(v,'linewidth',2);
hline(0,'k-');
h = vline(50.96,'b--'); % Angle of the proof of concept simulation
set(h,'linewidth',2);
ylim(y_lim);

set(gca,'fontsize',16);

%% Load the distance data

% Define all the thicknesses and number of runs
all_distance = 67:2:87;
all_n   = 20;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The cortical thickness
% 2. dimension: The runs
% 3. dimension: The layer model
Free_Energy = zeros(length(all_distance),all_n,2);

% Load all the files containing the free energy and save them
l_model = {'LC','LI'};
for i=1:length(all_distance)
    for j=1:all_n
        for k=1:2
            file_results =  ['..' filesep 'Data' filesep ...
                'Dipole_Properties' filesep ...
                'Distance' filesep  'Distance_' ...
                sprintf('%i',all_distance(i)) ...
                '__run_' sprintf('%i',j) '__' l_model{k} '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
                if j > 1
                    Free_Energy(i,j,k) = Free_Energy(i,j-1,k);
                end
            end
        end
    end
end

% Calculate the difference between the LC and LI model
delta_f = Free_Energy(:,:,1) - Free_Energy(:,:,2);

% Calculate the std and mean of the free energy
m_free_energy   = zeros(length(all_distance),1);
s_free_energy   = zeros(length(all_distance),1);
for i=1:length(all_distance)
 	m_free_energy(i) = mean(delta_f(i,:));
    s_free_energy(i) = std(delta_f(i,:));
end

%% Calculate the distance to the closest sensor
dip_loc = zeros(size(all_distance,2)+1,3);

% MNI coordinates of the dipole pair
dip_coord = [42 -29 56];
for i=1:size(all_distance,2)
    dip_loc(i,:) = dip_coord/norm(dip_coord)*all_distance(i);
end
dip_loc(end,:) = [42 -29 56];

% MNI to sensor space
MEEG = spm_eeg_load(['..' filesep 'Data' filesep ...
    'mce1spmeeg_s01_Uni_Rep1']);

% Get the sensor location in sensor space
sens_loc = MEEG.inv{1}.datareg.sensors.chanpos(1:274,:);

% Calculate the distance to each sensor
distance_sens_dipole = zeros(274,size(all_distance,2)+1);
for i=1:274
    for j=1:(size(all_distance,2)+1)
        distance_sens_dipole(i,j) = norm(sens_loc(i,:)-dip_loc(j,:));
    end
end

sens_distance = min(distance_sens_dipole);

%% Plot the distance subplot

subplot(3,1,3);
fill([sens_distance(1:end-1) fliplr(sens_distance(1:end-1))], ...
    [m_free_energy'+s_free_energy' ...
    fliplr(m_free_energy'-s_free_energy')], ...
    [.95 .95 .95], 'linestyle', 'none')
hold all
plot(sens_distance(1:end-1),m_free_energy','r-','Linewidth',2);
grid on
ylabel('{\Delta}F');
xlabel('Distance to closest sensor [mm]');
xlim([sens_distance(end-1),sens_distance(1)]);

% Get the y limit
y_lim = ylim;
v = hline(3,'g:');
set(v,'linewidth',2);
hline(0,'k-');
ylim(y_lim*2);
h = vline(sens_distance(end),'b--');
set(h,'linewidth',2);
ylim(y_lim);
set(gca,'fontsize',16);

h = suptitle( ...
    'Dipole Properties');
set(h,'FontSize',24);

%% Save the plot
saveas(gcf,'../Data/Dipole_Properties/dipole_properties.png')
saveas(gcf,'../Figures/dipole_properties.png')
saveas(gcf,'../Figures/dipole_properties.eps','epsc')
