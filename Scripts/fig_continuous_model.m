% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script creates the continuous_model figure. The data first has to be
% created by the ../Euler/continuous_model.sh script, which is using the
% data_continuous_model.m function.
%
clear all
close all

% Define all the SNRs and number of runs
all_SNR = [10 0 -10 -20];
all_n   = 20;
all_dd  = -2:0.5:4;

% Load the necessary TNU functions
addpath(genpath('../Functions/'));

% Create the Free_Energy variable, saving all the free energies
% 1. dimension: The SNRs
% 2. dimension: The runs
% 3. dimension: The dipole distance)
Free_Energy = zeros(length(all_SNR),all_n,length(all_dd));

% Load all the files containing the free energy and save them
l_model = {'LC','LD','LI'};
for i=1:length(all_SNR)
    for j=1:all_n
        for k=1:length(all_dd)
            file_results =  ['..' filesep 'Data' filesep ...
                'Continuous_Model' filesep 'SNR_' ...
                sprintf('%.2f',all_SNR(i)) '__run_' sprintf('%i',j) ...
                '__distance_' sprintf('%.1f',all_dd(k)) '.mat'];
            try
                load(file_results);
                Free_Energy(i,j,k) = F;
            catch
                [i j k]
                file_results
                if j > 1
                    Free_Energy(i,j,k) = Free_Energy(i,j-1,k);
                end
            end
        end
    end
end


% Calculate the difference between the 2mm model and the other models
delta_f = zeros(size(Free_Energy));
pos_0   = find(all_dd==2); % Position in Free energy, where dd = 2mm
for i=1:length(all_SNR)
    for j=1:all_n
        for k=1:length(all_dd)
            delta_f(i,j,k) = Free_Energy(i,j,k) - Free_Energy(i,j,pos_0);
        end
    end
end

% Calculate the std and mean of the free energy
m_free_energy   = zeros(length(all_dd),length(all_SNR));
s_free_energy   = zeros(length(all_dd),length(all_SNR));
for i=1:length(all_SNR)
 	for k=1:length(all_dd)
        m_free_energy(k,i) = mean(delta_f(i,:,k));
        s_free_energy(k,i) = std(delta_f(i,:,k))/sqrt(2-1);
	end
end

%% Plot the model

figure('units','normalized','outerposition',[0 0 1 1])

% Plot graphs
for i=1:length(all_SNR)
    % Do quadratic fit
    n = polyfit(all_dd,m_free_energy(:,i)',2);
    f = @(x) n(1).*(x.^2) + n(2).*x + n(3);
        
    % Print the percentage of Model, which is best fit and in dF of 3
    fprintf('SNR %.2f:\tBest Model at %.1fmm +- %.1fmm\n',all_SNR(i),...
    -0.5*n(2)/n(1),sqrt(abs(3/n(1))));

    subplot(2,2,i);
    fill([all_dd fliplr(all_dd)], ...
        [m_free_energy(:,i)'+s_free_energy(:,i)' ...
        fliplr(m_free_energy(:,i)'-s_free_energy(:,i)')], ...
        [.95 .95 .95], 'linestyle', 'none')
    hold all
    plot(all_dd,m_free_energy(:,i)','r-','Linewidth',2);

    title(sprintf('SNR of %.2f',all_SNR(i)));
    set(gca,'ygrid','on')
    ylabel('{\Delta}F');
    if(i>2)
        xlabel('Dipole Distance [mm]');
    end
    xlim([all_dd(1),all_dd(end)]);

    % Do not plot the parabolic approximation
    % plot(all_dd,f(all_dd),'b--','Linewidth',2);

    % Get the y limit
    y_lim = ylim;

    % Draw the region of delta F smaller than 3
    lower = -0.5*n(2)/n(1) - sqrt(abs(3/n(1)));
    upper = -0.5*n(2)/n(1) + sqrt(abs(3/n(1)));
    p=patch([lower lower upper upper], ...
    [y_lim(1) y_lim(2) y_lim(2) y_lim(1)],'r');
    set(p,'FaceAlpha',0.1); 
    set(p,'EdgeColor','None');


    v1 = vline(2,'k:');
    set(v1,'Linewidth',1.5);
%     if n(2) > 0
%         v2 = vline(-0.5*n(2)/n(1),'g:');
%         set(v2,'Linewidth',1.5);
%     end
    ylim(y_lim);
    set(gca,'fontsize',16);
end
h = suptitle( ...
    'Free Energy Relative to Model with Dipole Distance = 2mm');
set(h,'FontSize',24);

%% Save the plot
saveas(gcf,'../Data/Continuous_Model/continuous_model.png')
saveas(gcf,'../Figures/continuous_model.png')
saveas(gcf,'../Figures/continuous_model.eps','epsc')
