% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function data_cortical_thickness(thickness,n,orientation)
% This code is doing a single simulation used for the
% fig_dipole_properties. The cortical thickness, the noise signal (depenend
% on n) and the orientation (LC, LD, LI) can be defined.
% 
% The Free Energy is saved in the following output file:
% ../Data/Dipole_Properties/Cortical_Thickness/...
% In the file there are the variables:
% -) Ep                 - Output from spm_nlsi_GN
% -) Cp                 - Output from spm_nlsi_GN
% -) Eh                 - Output from spm_nlsi_GN
% -) F                  - Free enrgy from spm_nlsi_GN
% -) real_thickness     - (0, 0.5, 1, ..., 6)
% -) real_n             - run number (1, 2, ..., 20)
% -) real_orientation   - Orientation (-1, 0, 1)
% 
% INPUT:
%
% thickness     - Cortical thickness in mm
% n             - Noise trace (1, 2, ..., 20)
% orientation   - Define the Layer model (LC, LD, or LI):
%                   -) -1           - Layer Correct Model
%                   -) 0            - Layer Detached Model
%                   -) 1            - Layer Inverted Model
% 

    %% Initizalization    
    % Add the SPM12 package and all other necessary directories to the
    % MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));

    % Initialize the random generator
    rng(n+100*thickness);

    % Change the step size for numerical derivatives
    global GLOBAL_DX
    GLOBAL_DX = exp(-5);
    
    % Define the possible SNRs and number of runs
    all_thickness = 0:1/3:4;
    
    % Define the results file filename
    file_results =  ['..' filesep 'Data' filesep 'Dipole_Properties' ...
       filesep 'Cortical_Thickness' filesep 'Thickness_' ... 
       sprintf('%.1f',all_thickness(thickness)) '__run_' sprintf('%i',n)];
    
    SNR_level        = load(['..' filesep 'Data' filesep ...
        'Jones_Compartmental_Model' filesep ...
        'SNR_level.mat']);
    SNR_level        = SNR_level.SNR_level;
                                          % This is equivalent to the model
                                          % used in the proof of concept 
                                          % with a SNR of 0
    
    real_thickness   = all_thickness(thickness)
    real_n           = n
    real_orientation = orientation
    
    switch orientation
        case -1
            file_results = [file_results '__LI.mat'];
            disp('LI');
        case 0
            file_results = [file_results '__LD.mat'];
            disp('LD');
        case 1
            file_results = [file_results '__LC.mat'];
            disp('LC');
    end
    
    %% Define parameter
    u_dt      = 1/2400;  % Resolution of the state equation in sec
    y_dt      = 1/2400;  % Resolution of the observation equation in sec
    t_stop    = 0.160;   % Length of simulation in sec



    % MNI coordinates of the dipole pair
    dip_coord = [42 -29 56]; 

    % Orienation of the dipole pair. The information is given in form of a 
    % normal vector in the MNI coordinate system
    dip_ori   = tnu_create_orientation(dip_coord,[1 0 0],60);

    % Distance between the two dipoles of a dipole pair in mm. The position
    % difference is given as (loc_L23 - loc_L5)
    dip_loc   = real_thickness * dip_ori / norm(dip_ori); 

    % Define the kind of dimensionality reduction used in this simulation
    dim_red   = 'none';

    %% Create the input signal
    ff_mean   = 0.025;   % Mean of FF in sec
    fb_mean   = 0.070;   % Mean of FB in sec
    ff2_mean  = 0.135;   % Mean of FF2 in sec

    ff_std    = 0.0025;  % Standard deviation of FF in sec
    fb_std    = 0.006;   % Standard deviation of FB in sec
    ff2_std   = 0.007;   % Standard deviation of FF2 in sec

    U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
        ff_std,fb_std,ff2_std);

    %% Create the model used for the simulation and inversion
    M_sim     = tnu_create_model(u_dt,y_dt,t_stop, ...
        dip_coord,dip_ori,dip_loc, ...
        dim_red);

    M_inv     = tnu_create_model(u_dt,y_dt,t_stop, ...
        dip_coord,dip_ori,dip_loc*orientation, ...
        dim_red);
    
    % Load the data used for simulating the traces
    simulating_E = load(['..' filesep 'Data' filesep ...
        'Jones_Compartmental_Model' filesep ...
        'CMCM_fitted_to_JCM_parameter.mat']);
    simulating_E = simulating_E.Ep;
    
    
    %% Simulation
    Y.dt      = y_dt;
    Y.y       = spm_int(simulating_E,M_sim,U);
    
    % Add noise
    Y         = tnu_add_noise(Y,SNR_level,'abs');
    
    %% Inversion
    [Ep,Cp,Eh,F] = spm_nlsi_GN(M_inv,U,Y);
    F
    
    %% Save Data
    save(file_results,'Ep','Cp','Eh','F','real_thickness','real_n', ...
        'real_orientation');
    
    %% Termination
    % Remove all directories from the MATLAB path
    addpath(genpath('../spm12/'));
    addpath(genpath('../Functions/'));
end
