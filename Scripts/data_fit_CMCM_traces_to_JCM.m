% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script is fitting the CMC model to the traces of the JCM.
%

%% Initizalization
clear all
close all

% Add the SPM12 package and all other necessary directories to the MATLAB
% path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

% Initialize the random generator
rng(0);

% Change the step size for numerical derivatives
global GLOBAL_DX
GLOBAL_DX = exp(-5);

%% Define parameter
u_dt      = 0.0005;  % Resolution of the state equation in sec
y_dt      = 0.0005;  % Resolution of the observation equation in sec
t_stop    = 0.160;   % Length of simulation in sec

% MNI coordinates of the dipole pair (not necessary)
dip_coord = [0 0 0]; 

% Orienation of the dipole pair. The information is given in form of a 
% normal vector in the MNI coordinate system (not necessary)
dip_ori   = [1 1 1];

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5) (not necessary)
dip_loc   = 2 * dip_ori / norm(dip_ori); 

% Define the kind of dimensionality reduction used in this simulation
dim_red   = 'traces';

%% Create the input signal
ff_mean   = 0.025;   % Mean of FF in sec
fb_mean   = 0.070;   % Mean of FB in sec
ff2_mean  = 0.135;   % Mean of FF2 in sec

ff_std    = 0.0025;  % Standard deviation of FF in sec
fb_std    = 0.006;   % Standard deviation of FB in sec
ff2_std   = 0.007;   % Standard deviation of FF2 in sec

U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
    ff_std,fb_std,ff2_std);

%% Create the CMC model
M         = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,dip_loc, ...
    dim_red);

%% Load the JCM traces

load('../Data/Jones_Compartmental_Model/output_traces');
l2 = L2_JCM(1:round(y_dt/(t_JCM(1))):round(1+t_stop/t_JCM(1)));
l5 = L5_JCM(1:round(y_dt/(t_JCM(1))):round(1+t_stop/t_JCM(1)));

Y_JCM.dt      = y_dt;
Y_JCM.y       = [l2, l5];

%% Invert the CMC model
M.nograph     = 0;
[Ep,Cp,Eh,F]  = spm_nlsi_GN(M,U,Y_JCM);
Y_CMCM        = spm_int(Ep,M,U);

fprintf('\n\nFree Energy: %.2f\n\n',F);

%% Display the results
figure('units','normalized','outerposition',[0 0 1 1])

fontsize = 16;

subplot(1,2,1);
plot(0:y_dt:t_stop,l2,'r-',0:y_dt:t_stop,Y_CMCM(:,1),'b-', ...
    'linewidth',2);
set(gca,'fontsize',fontsize);
xlim([0,0.160]);
title('Layer II/III','fontsize',fontsize);
xlabel('Time [sec]','fontsize',fontsize);
ylabel('Signal strength [pAm]','fontsize',fontsize);
grid on

subplot(1,2,2);
plot(0:y_dt:t_stop,l5,'r-','linewidth',2);
hold on
plot(0:y_dt:t_stop,Y_CMCM(:,2),'b-','linewidth',2)
set(gca,'fontsize',fontsize);
xlim([0,0.160]);
lgd = legend('JCM','CMC');
lgd.FontSize = fontsize;
title('Layer V','fontsize',fontsize);
xlabel('Time [sec]','fontsize',fontsize);
ylabel('Signal strength [pAm]','fontsize',fontsize);
grid on

%% Save the results
save(['..' filesep 'Data' filesep 'Jones_Compartmental_Model' filesep ...
   'CMCM_fitted_to_JCM_parameter.mat'], 'Ep');
saveas(gcf,'../Figures/JCM_to_CMCM.png')
saveas(gcf,'../Figures/JCM_to_CMCM.eps','epsc')

%% Termination
% Remove all directories from the MATLAB path
rmpath(genpath('../spm12/'));
rmpath(genpath('../Functions/'));
