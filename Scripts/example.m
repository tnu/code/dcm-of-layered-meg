% Author: Stephan IHLE and Jakob HEINZLE
% Created: 09.03.2019
% Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% This script is introducing a simple introduction into how to use this
% code package.
%

%% Initizalization
clear all
close all

% Add the SPM12 package and all other necessary directories to the MATLAB
% path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));

% Initialize the random generator
rng(0);

% Change the step size for numerical derivatives
global GLOBAL_DX
GLOBAL_DX = exp(-5);

%% Define parameter
u_dt      = 0.0005;  % Resolution of the state equation in sec
y_dt      = 0.01;    % Resolution of the observation equation in sec
t_stop    = 0.160;   % Length of simulation in sec



% MNI coordinates of the dipole pair
dip_coord = [43 -29 56]; 

% Orienation of the dipole pair. The information is given in form of a 
% normal vector in the MNI coordinate system
dip_ori   = [-1 1 0];

% Distance between the two dipoles of a dipole pair in mm. The position
% difference is given as (loc_L23 - loc_L5)
dip_loc   = 2 * dip_ori / norm(dip_ori); 

% Define the kind of dimensionality reduction used in this simulation
dim_red   = 'none';

%% Create the input signal
ff_mean   = 0.025;   % Mean of FF in sec
fb_mean   = 0.070;   % Mean of FB in sec
ff2_mean  = 0.135;   % Mean of FF2 in sec

ff_std    = 0.0025;  % Standard deviation of FF in sec
fb_std    = 0.006;   % Standard deviation of FB in sec
ff2_std   = 0.007;   % Standard deviation of FF2 in sec

U         = tnu_create_input(u_dt,t_stop,ff_mean,fb_mean,ff2_mean, ...
    ff_std,fb_std,ff2_std);

%% Create the LC and LI model
M_LC      = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,dip_loc, ...
    dim_red);

M_LI      = tnu_create_model(u_dt,y_dt,t_stop, ...
    dip_coord,dip_ori,-dip_loc, ...
    dim_red);

%% Plot the Sensor traces of the pial dipole in the LC model
figure(1);
hold on

strength = M_LC.l_pial/max(abs(M_LC.l_pial));

% Get the sensor location and enable the transformation
D        = spm_eeg_load( ...
	['..' filesep 'Data' filesep 'mce1spmeeg_s01_Uni_Rep1.mat']);
sensors  = D.inv{1}.forward.sensors.chanpos(1:274,:);

% Plot the sensors
for i=1:274
    col  = full([(max(0,strength(i))) 0 (-min(0,strength(i)))]);
    temp_point = tnu_transform_sensor2MNI(D,sensors(i,:));
    plot3(temp_point(1),temp_point(2),temp_point(3),'o','Color',col, ...
        'LineWidth',5);
end

% Plot the brain (not used in our case)
vertices = D.inv{1}.mesh.tess_mni.vert;
for i=1:10:size(vertices,1)
    plot3(vertices(i,1),vertices(i,2),vertices(i,3),'ok', ...
        'LineWidth',1);
end

% Plot the dipole location and orientation
plot3(dip_coord(1),dip_coord(2),dip_coord(3),'xy','LineWidth',10);
plot3([dip_coord(1) dip_coord(1)+dip_ori(1)/norm(dip_ori)*20], ...
    [dip_coord(2) dip_coord(2)+dip_ori(2)/norm(dip_ori)*20], ...
    [dip_coord(3) dip_coord(3)+dip_ori(3)/norm(dip_ori)*20], ...
    'g-','LineWidth',5);

% Plot the origin of the MNI coordinate system
plot3(0,0,0,'xm', ...
    'LineWidth',10);


%% Simulate the traces with the LC model and add noise
SNR       = 10;

Y.dt      = y_dt;
Y.y       = spm_int(M_LC.pE,M_LC,U);

Y         = tnu_add_noise(Y,SNR);
% st        = 5.1972e-08
% Y         = tnu_add_noise(Y,st,'abs');

%% Invert both the LC and LI model
[Ep_LC,Cp_LC,Eh_LC,F_LC] = spm_nlsi_GN(M_LC,U,Y);
[Ep_LI,Cp_LI,Eh_LI,F_LI] = spm_nlsi_GN(M_LI,U,Y);

%% Display the results
fprintf('\n\nF_LC - F_LI = %.2f\n\n',F_LC-F_LI);

%% Termination
% Remove all directories from the MATLAB path
addpath(genpath('../spm12/'));
addpath(genpath('../Functions/'));
