# DCM of layered MEG

Dynamic causal modeling of layered magnetoencephalographic event-related responses.

## Installing the Git Repository

The following is a step-by-step introduction how this git repository needs to be installed. It is written for a GNU/Linux computer. However, the steps for a Windows and Mac PC will be identical.

1. Make sure, [MATLAB](https://www.mathworks.com) and [NEURON](https://www.neuron.yale.edu/neuron/) are running on your computer.
2. Clone this git and cd into the root directory: <pre>cd dcm-of-layered-meg</pre>
3. Download the FieldTrip toolbox from the [FieldTrip website](http://www.fieldtriptoolbox.org/). In this work, the version fieldtrip-20190912 was used.
4. Make sure that the unpacked fieldTrip folder is placed in the root directory of this git.
5. Download the [SPM software](https://www.fil.ion.ucl.ac.uk/spm/software/spm12/) and unzip it. We used version 6906.
6. Move the unpacked spm12 folder to the root directory of this git.

The following steps are optional. They can be done in order to fit a CMCM to the compartmental model of [Jones et al.](https://www.ncbi.nlm.nih.gov/pubmed?holding=modeldb&term=17913909). Such a trained CMCM has been added to the git. 

1. Download the MEG model of the Somatosensory Neocortex as introduced by [Jones et al.](https://www.ncbi.nlm.nih.gov/pubmed?holding=modeldb&term=17913909) from [ModelDB (SenseLab)](https://senselab.med.yale.edu/ModelDB/default.cshtml).
2. Extract this MEG model and make sure the resulting folder is placed in the root directory of this git.
3. Rename the folder created in the step before to **Jones_et_al_Model**, while at the same time removing nested subfolders. E.g.: <pre>mv SS-cortex_r2/SS-cortex Jones_et_al_Model</pre>
4. cd into Jones_et_al_Model and execute the following command: <pre>nrnivmodl ar.mod cat.mod dpresyn.mod kca.mod na.mod cad.mod catp.mod gnetstim.mod kdr.mod pp_dipole.mod tchannel.mod ca.mod dipole.mod h.mod km.mod precall.mod</pre>
5. Run batch.hoc to create 100 trial examples: <pre>nrniv batch.hoc -</pre> This should create a file called **suprathresh.dat**.
6. cd into the Scripts folder and execute first the matlab script data_create_JCM_traces.m: <pre>matlab data_create_JCM_traces.m</pre>
7. Then, execute the matlab script data_fit_CMCM_traces_to_JCM.m in the same folder: <pre>matlab data_fit_CMCM_traces_to_JCM.m</pre> This will train the CMCM used as the forwad model in all simulations.

# Newer version of MATLAB:

The code was written for MATLAB 2015a and 2016b. For some newer MATLAB versions, the subtitle(.) command must be replaced with sgtitle(.). Other changes may also be necessary.