# Author: Stephan IHLE and Jakob HEINZLE
# Created: 09.03.2019
# Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
#
# This file is released under the terms of the GNU General Public
# Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
# (either version 3 or, at your option, any later version). 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cd ../Scripts

length=11
n=20
for j in `seq 1 $n`
do
  	echo $j
        n=$(bjobs | wc -l)
        while [ $n -gt 20000 ]
        do
          	n=$(bjobs | wc -l)
                sleep 10
        done
	for i in `seq 1 $length`
        do
          	sleep 1
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__sum__LC.log matlab -r "data_dimensionality_reduction($i,$j,1,0); exit" &
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__sum__LD.log matlab -r "data_dimensionality_reduction($i,$j,0,0); exit" &
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__sum__LI.log matlab -r "data_dimensionality_reduction($i,$j,-1,0); exit" &
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__diff__LC.log matlab -r "data_dimensionality_reduction($i,$j,1,1); exit" &
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__diff__LD.log matlab -r "data_dimensionality_reduction($i,$j,0,1); exit" &
                bsub -R "rusage[mem=2048]" -W 13:59 -o ../Data/Dimensionality_Reduction/DR_$(echo $i)__run_$(echo $j)__diff__LI.log matlab -r "data_dimensionality_reduction($i,$j,-1,1); exit" &
        done
	wait
done
