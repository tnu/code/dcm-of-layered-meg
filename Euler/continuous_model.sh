# Author: Stephan IHLE and Jakob HEINZLE
# Created: 09.03.2019
# Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
#
# This file is released under the terms of the GNU General Public
# Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
# (either version 3 or, at your option, any later version). 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cd ../Scripts

SNR=4
n=20
dd=13
for j in `seq 1 $n`
do
  	echo $j
        n=$(bjobs | wc -l)
        while [ $n -gt 20000 ]
        do
          	n=$(bjobs | wc -l)
                sleep 10
        done
	for i in `seq 1 $SNR`
    do
		for k in `seq 1 $dd`
			do
				sleep 1
				bsub -R "rusage[mem=4095]" -W 13:59 -o ../Data/Continuous_Model/SNR_$(echo $i)__run_$(echo $j)__distance_$(echo $k).log matlab -r "data_continuous_model($i,$j,$k); exit" &
			done
		wait
    done
done
