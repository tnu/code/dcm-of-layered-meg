# Author: Stephan IHLE and Jakob HEINZLE
# Created: 09.03.2019
# Copyright (C) 2019 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
#
# This file is released under the terms of the GNU General Public
# Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
# (either version 3 or, at your option, any later version). 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

sh angle.sh
sh continuous_model.sh
sh cortical_thickness.sh
sh dimensionality_reduction.sh
sh distance.sh
sh lfp_norm.sh
sh orientation_estimation.sh
sh proof_of_concept.sh
sh stability_test.sh
